package com.hi.hi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jgit.api.errors.GitAPIException;

import com.gitee.ApiException;
import com.gitee.clone.*;

/**
 * 运行 git-downloader 
 * @author fu
 *
 */
public class Run {
	
	public Run(String confPath) throws GitAPIException, Exception {
		readConf(confPath);
		run(confPath);
	}
	
	/**
	 * 空，以备以后需要
	 * @param confPath
	 */
	public void readConf(String confPath) {
	}
	
	/**
	 * 获取配置并运行 git-downloader 
	 * @param confPath	配置路径
	 * @throws GitAPIException	API错误
	 * @throws Exception		其他错误
	 */
	public void run(String confPath) throws GitAPIException, Exception {
		PropertiesCom pps = new PropertiesCom();
		pps.readProperties(confPath);
		
		if (pps.get("mode").equals("diff")) {
			getDiff(pps.get("org"), pps.get("userName"), pps.get("passWord"), pps.get("localPath"),
					pps.get("url"), pps.get("accessToken"),pps.get("type"));
		}else if(pps.get("mode").equals("new")) {
			gitClone(pps.get("org"), pps.get("userName"), pps.get("passWord"), pps.get("localPath"),
					pps.get("url"), pps.get("accessToken"),pps.get("type"));
		}else {
			throw new Exception("mode数值错误， 现有值为：" + pps.get("mode"));
		}

	}
	
	/**
	 * 仓库克隆更新
	 * 
	 * @param org         仓库所属空间地址（*）
	 * @param userName    远程服务器上的账号（*）
	 * @param passWord    远程服务器上的密码（*）
	 * @param localPath   远程库存放的路径（*）
	 * @param Url         远程服务器地址（*）
	 * @param accessToken 用户授权码
	 * @param type        筛选仓库的类型
	 * @throws ApiException If fail to call the API
	 * @throws Exception    缺少必要参数
	 */
	void gitClone(String org, String userName, String passWord, String localPath, String Url, String accessToken,
			String type) throws ApiException, Exception {
		if (org == null)
			throw (new Exception("lose org"));
		if (userName == null)
			throw (new Exception("lose userName"));
		if (passWord == null)
			throw (new Exception("lose passWord"));
		if (localPath == null)
			throw (new Exception("lose localPath"));
		if (Url == null)
			throw (new Exception("lose Url"));
		if (type == null)
			type = "all";

		Repository repository = new Repository();
		repository.setOrg(org); // String | 组织的路径(path/login)
		repository.setAccessToken(accessToken); // String | 用户授权码
		repository.setType(type); // String | 筛选仓库的类型，可以是 all, public, private。默认: all
		repository.setPerPage(100); // Integer | 每页的数量，最大为 100
		repository.setPage(1); // Integer | 当前的页码
		repository.setUrl(Url); // String | 仓库的路径(path/login)
		GitClone.gitClone(repository, userName, passWord, localPath); 

	}

	/**
	 * 仓库增量更新
	 * 
	 * @param org         仓库所属空间地址（*）
	 * @param userName    远程服务器上的账号（*）
	 * @param passWord    远程服务器上的密码（*）
	 * @param localPath   远程库存放的路径（*）
	 * @param Url         远程服务器地址（*）
	 * @param accessToken 用户授权码
	 * @param type        筛选仓库的类型
	 * @throws ApiException    If fail to call the API
	 * @throws IOException
	 * @throws GitAPIException
	 * @throws Exception       缺少必要参数
	 */
	void getDiff(String org, String userName, String passWord, String localPath, String Url, String accessToken,
			String type) throws ApiException, IOException, GitAPIException, Exception {
		if (org == null)
			throw (new Exception("lose org"));
		if (userName == null)
			throw (new Exception("lose userName"));
		if (passWord == null)
			throw (new Exception("lose passWord"));
		if (localPath == null)
			throw (new Exception("lose localPath"));
		if (Url == null)
			throw (new Exception("lose Url"));
		if (type == null)
			type = "all";
		Repository repository = new Repository();
		repository.setOrg(org); // String | 组织的路径(path/login)
		repository.setAccessToken(accessToken); // String | 用户授权码
		repository.setType(type); // String | 筛选仓库的类型，可以是 all, public, private。默认: all
		repository.setPerPage(100); // Integer | 每页的数量，最大为 100
		repository.setPage(1); // Integer | 当前的页码
		repository.setUrl(Url); // String | 仓库的路径(path/login)
		String sha = "master";
		Integer recursive = null;
		GetDiff.getDiff(repository, sha, recursive, userName, passWord, localPath);
	}

	
}
