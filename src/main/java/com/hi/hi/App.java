package com.hi.hi;

import com.gitee.ApiException;
import com.gitee.api.RepositoriesApi;
import com.gitee.clone.*;
import com.gitee.model.Project;
import org.eclipse.jgit.api.Git;


import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
/**
 * 克隆远程仓库
 */
public class App {
	
	AllConf conf = new AllConf();
	
	public App() {
		//initMap();
	}
	
	/**
	 * 克隆远程仓库
	 * 
	 * @param args
	 * @throws ApiException If fail to call the API
	 */
	public static void main(String[] args) {
		App app=new App();
		app.run(args);

	}
	
	public void run(String[] args) {
		new CommandLine().readCom(args, conf);
	}
	

}

