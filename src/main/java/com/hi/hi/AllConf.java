package com.hi.hi;

public class AllConf {
	//
	String cronFile;
	//
	String gitConf;
	
	String cronCom;
	
	String cronTime;
	
	public AllConf() {
		cronFile = System.getProperty("user.home") + "/.git-downloader/cron";
		gitConf = System.getProperty("user.home") + "/.git-downloader/git";
		cronTime = "0 0 * * * ";
		cronCom = "";
	}
	
	public void setCronfile(String cronfile) {
		cronFile = cronfile;
	}
	
	public String getCronfile() {
		return cronFile;
	}

	
	public void setGitConf(String gitconf) {
		gitConf = gitconf;
	}
	
	public String getGitConf() {
		return gitConf;
	}
	
	public void addCronCom(String croncom) {
		cronCom += croncom;
	}
	
	public String getCronCom() {
		return cronCom;
	}
	
	public void setCronTime(String crontime) {
		cronTime = crontime;
	}
	
	public String getCronTime() {
		return cronTime;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("AllConf :").append(System.getProperty("line.separator"))
		.append("	cronFile =" + cronFile).append(System.getProperty("line.separator"))
		.append("	gitConf =" + gitConf).append(System.getProperty("line.separator"))
		.append("	cronCom =" + cronCom).append(System.getProperty("line.separator"))
		.append("	cronTime =" + cronTime).append(System.getProperty("line.separator"));
		
		return sb.toString();
	}
}
