package com.hi.hi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

import com.gitee.ApiException;

/**
 * Properties 相关所有内容
 * @author fu
 *
 */
public class PropertiesCom {

	String passWord,accessToken,type,localPath,userName,Url,org,mode;
	

	/**
	 * 读取本地Properties文件
	 * @param filePath	文件路径
	 * @throws IOException
	 */
	public void readProperties (String filePath) throws IOException {
		Properties pps = new Properties();		 
		InputStream in = new FileInputStream(filePath);

		//从输入流中读取属性列表（键和元素对）
		pps.load(in);
		
		passWord = pps.getProperty("passWord");
		accessToken = pps.getProperty("accessToken");
		type = pps.getProperty("type");
		localPath = pps.getProperty("localPath");
		userName = pps.getProperty("userName");
		Url = pps.getProperty("Url");
		org = pps.getProperty("org");
		mode = pps.getProperty("mode");

	}
	
	/**
	 * 生成所需的git配置文件
	 * @param filePath	本地文件路径
	 * @throws IOException
	 */
	public static void NewProperties (String filePath) throws IOException {

		OutputStream out = new FileOutputStream(filePath);
		
		StringBuilder sb = new StringBuilder();
		Date date = new Date();
		sb.append("#A new Properties file").append(System.lineSeparator())
		.append("#").append(date.toString()).append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 运行模式  new | diff").append(System.lineSeparator())
		.append("mode=diff").append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 仓库所属空间地址（*）").append(System.lineSeparator())
		.append("org=").append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 远程服务器上的账号（*）").append(System.lineSeparator())
		.append("userName=").append(System.lineSeparator())
		
		.append("# 远程服务器上的密码（*）").append(System.lineSeparator())
		.append("passWord=").append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 远程库存放的本地路径（*）").append(System.lineSeparator())
		.append("localPath=").append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 远程服务器地址（*）").append(System.lineSeparator())
		.append("Url=https://gitee.com/").append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 用户授权码").append(System.lineSeparator())
		.append("accessToken=").append(System.lineSeparator())
		.append(System.lineSeparator())
		
		.append("# 筛选仓库的类型").append(System.lineSeparator())
		.append("type=all").append(System.lineSeparator());
		
		
		out.write(sb.toString().getBytes());
		out.close();
	}
	
	/**
	 * get all
	 * (我懒了，谁想一个个写谁改吧= =)
	 * @param Name	参数名
	 * @return
	 */
	public String get(String Name) {
		switch(Name) {
		case "passWord":
			return passWord;
		case "accessToken":
			return accessToken;
		case "type":
			return type;
		case "localPath":
			return localPath;
		case "userName":
			return userName;
		case "url":
			return Url;
		case "org":
			return org;
		case "mode":
			return mode;
		default:
			return null;
		}
	}
	
	/**
	 * to String
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("PropertiesCom :").append(System.lineSeparator())
		.append("	mode =" + mode).append(System.lineSeparator())
		.append("	passWord =" + passWord).append(System.lineSeparator())
		.append("	accessToken =" + accessToken).append(System.lineSeparator())
		.append("	type =" + type).append(System.lineSeparator())
		.append("	localPath =" + localPath).append(System.lineSeparator())
		.append("	userName =" + userName).append(System.lineSeparator())
		.append("	Url =" + Url).append(System.lineSeparator())
		.append("	org =" + org).append(System.lineSeparator());
		
		return sb.toString();
	}
}









