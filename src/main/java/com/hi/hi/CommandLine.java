package com.hi.hi;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.eclipse.jgit.api.errors.GitAPIException;

import com.gitee.ApiException;

public class CommandLine {
	

	public void readCom(String[] command, AllConf allConf) {
		
		words:  
		for(int i = 0 ; i < command.length ; i++) {
			if (command[i].startsWith("-")) {
				String[] conf = command[i].split("");
				
				word:  
				for (String c : conf) {
					switch (c) {
					case "h":
						showHelp();
						return;
					case "f":
						allConf.setCronfile(command[++i]);
						break word;
					case "g":
						allConf.setGitConf(command[++i]);
						break word;
					case "a":
						allConf.addCronCom(" add");
						break;
					case "t":
						allConf.setCronTime(command[++i]);
						break word;
					case "r": 
						allConf.addCronCom(" remove");
						break;
					case "R": 
						allConf.addCronCom(" removeAll");
						break;
					case "l": 
						allConf.addCronCom(" list");
						break;
					}
				}
			}
		}
		Cron cron;
		try {
			switch (command[0]) {
			case "help":
				showHelp();
				break;
			case "run":
				new Run(allConf.getGitConf());
				break;
			case "creatfile":
				creatfile(allConf.getGitConf());
				break;
			case "cron":
				cron = new Cron();
				cron.common(allConf);
				break;
			case "test":
				System.out.println(allConf.toString());
				System.out.println("test cron");
				allConf.addCronCom(" add list remove add removeAll");
				cron = new Cron();
				cron.common(allConf);
				
				System.out.println("test creatfile");
				creatfile(allConf.getGitConf());
				
				System.out.println("test help");
				showHelp();
				
				String path = CommandLine.class.getProtectionDomain().getCodeSource().getLocation().getFile();
				System.out.println("路径 java.class.path 为 ：" + System.getProperty("java.class.path"));
				System.out.println("路径 class.getProtectionDomain.getCodeSource.getLocation.getFile 为 ：" + path);
				System.out.println("换行符效果 ： 换行前" + System.lineSeparator() + "换行后");
				break;
			default:
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 生成git配置文件
	 * @param gitpath	git配置文件路径
	 * @throws IOException	文件写入错误
	 */
	public void creatfile (String gitpath) throws IOException {
		File git = new File(gitpath);
		if (!git.exists()) {
			if (git.getParentFile() != null && !git.getParentFile().exists()) 
			    git.getParentFile().mkdirs();
			git.createNewFile();
		}
		System.out.println("创建git配置文件： " + gitpath);
		PropertiesCom.NewProperties(gitpath);
	} 

	/**
	 * 帮助页
	 */
	public void showHelp() {
		StringBuilder sb = new StringBuilder();
		sb.append("hi~").append(System.lineSeparator())
		.append("命令格式： java -jar 本包 动作 -参数 [数据] -参数 [数据] ... ").append(System.lineSeparator())
		.append("需要额外数据的参数后需紧跟数据，参数与数据之间的内容将被丢弃。\n")
		.append("\n")
		.append("动作：").append("\n")
		.append("creatfile	创建git配置文件（地址受参数影响）").append(System.lineSeparator())
		.append("help		显示帮助").append(System.lineSeparator())
		.append("run		手动运行差异更新").append(System.lineSeparator())
		.append("cron		cron操作").append(System.lineSeparator())
		.append("\n")
		.append("参数(无需额外数据)：").append(System.lineSeparator())
		.append("h		显示帮助").append(System.lineSeparator())
		.append("a		添加cron任务").append(System.lineSeparator())
		.append("r		清除一个cron任务").append(System.lineSeparator())
		.append("R		清除所有cron任务").append(System.lineSeparator())
		.append("l		显示所有cron任务").append(System.lineSeparator())
		.append("\n")
		.append("参数(需要额外数据)：").append(System.lineSeparator())
		.append("c		设置corn文件路径（默认:~/.git-downloader/corn）").append(System.lineSeparator())
		.append("g		设置git文件路径（默认:~/.git-downloader/git）").append(System.lineSeparator())
		.append("t		设置cron任务频率（默认: 0 0 * * *）").append(System.lineSeparator());
		System.out.println(sb.toString());
	}
	

}
