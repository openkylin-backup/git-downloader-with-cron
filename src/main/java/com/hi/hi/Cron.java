package com.hi.hi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class Cron {
	
	String localPath ;
	
	public void common(AllConf conf) {
		localPath = Cron.class.getProtectionDomain().getCodeSource().getLocation().getFile();
		String[] cmds = conf.getCronCom().split(" ");

		for (String cmd : cmds) {
			switch (cmd) {
			case "add":
				add(conf);
				break;
			case "remove":
				remove(conf);
				break;
			case "removeAll":
				removeAll();
				break;
			case "list":
				list();
				break;
			default:
			}
		}
	}

	/**
	 * 添加 Cron 任务
	 * @param conf 设定
	 */
	public void add(AllConf conf) {
		String oldCron = getOldCron();

		// * * * * * asd.jar run -g ~/.git-downloader/git  
		String newCron = oldCron + conf.getCronTime() + " " + localPath
				+ " run -g " + conf.getGitConf() + System.lineSeparator();
		
		writeFile(conf.getCronfile(), newCron);
		
		System.out.println("add success.");
	}
	
	/**
	 * 等效于“crontab -l”
	 */
	public void list() {
		try {
			System.out.println("crontab -l : ");
			System.out.println(doShell("crontab -l"));
		} catch (Exception e) {
			System.out.println("命令\"crontab -l\"运行错误 ");
			e.printStackTrace();
		}
	}
	
	/**
	 * 等效于“crontab -r”
	 */
	public void removeAll() {
		try {
			System.out.println(doShell("crontab -r"));
		} catch (Exception e) {
			System.out.println("命令\"crontab -r\"运行错误 ");
			e.printStackTrace();
		}
		System.out.println("remove all success.");
	}
	
	/**
	 * 删除 Cron 任务
	 * @param conf 设定
	 */
	public void remove(AllConf conf) {
		String[] oldCrons = getOldCron().split(System.lineSeparator());
		
		// * * * * * asd.jar run -g ~/.git-downloader/git
		String rmCron = conf.getCronTime() + " " + localPath
				+ " run -g " + conf.getGitConf();
		
		String newCron = "";
		for(String oldCron : oldCrons) {
			if( !oldCron.equals(rmCron)){
				newCron += oldCron + System.lineSeparator();
			}
		}
		
		writeFile(conf.getCronfile(), newCron);
		
		System.out.println("remove success.");
	}
	
	/**
	 * 获取之前的 cron
	 * @return cron服务文本
	 */
	private String getOldCron() {
		String oldCron;
		try {
			oldCron = doShell("crontab -l");
		} catch (Exception e) {
			System.out.println("命令\"crontab -l\"运行错误 ");
			e.printStackTrace();
			return null;
		}
		if (oldCron.startsWith("no crontab"))
			oldCron = "";
		return oldCron;
	}
	
	/**
	 * 写入文件 / 旧文件将会被删除
	 * @param CronPath	文件路径
	 * @param newcron	写入的内容
	 */
	private void writeFile(String CronPath, String newcron) {
		
		File cron = new File(CronPath);
		
		OutputStream out = null;
		try {
			if (cron.getParentFile() != null && !cron.getParentFile().exists()) 
				cron.getParentFile().mkdirs();
			
			cron.delete();
			cron.createNewFile();
			
		
			out = new FileOutputStream(CronPath);
			out.write(newcron.getBytes());
			out.close();
			
			doShell("crontab " + CronPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("文件 :" + cron.getPath() + " 创建错误 ");
			e.printStackTrace();
			return;
		} catch (InterruptedException e) {
			System.out.println("命令\"crontab " + CronPath + "\"运行错误 ");
			e.printStackTrace();
		}finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	//protected String set

	/**
	 * 运行 shell 命令 / 无法运行复杂命令
	 * @param cmd	命令
	 * @return		结果
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private String doShell(String cmd) throws IOException, InterruptedException {
		Process p = Runtime.getRuntime().exec(cmd);
		InputStream is = p.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line, lines = "";

		while ((line = reader.readLine()) != null) {
			lines += System.lineSeparator() + line;
		}
		p.waitFor();
		is.close();
		reader.close();
		p.destroy();
		
//		System.out.println("执行命令 ：" + cmd);
//		System.out.println("结果为 ：" + lines);
		return lines;
	}

}
