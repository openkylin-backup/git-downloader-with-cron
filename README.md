# git-downloader-with-cron

#### 介绍
基于crontab的后台git-downloader包， 依赖于git-downloader和crontab。


#### 安装教程

如果你希望自行修改并编译成包的话

1.  安装 git-downloader ：
	- `git clone https://gitee.com/openkylin/git-downloader.git ； cd git-downloader` 
	- `mvn install`
2.  下载本包并进行修改
3.  修改pom.xml文件中的版本信息
4.  `mvn package` 打包

如果你只希望使用他的话

1.  确保Java版本不低于1.7
2.  下载本包，cron-downloader-1.0.0.jar就是你所有需要的

#### 使用说明

你可以使用命令：

`java -jar [yourPath].jar`

来运行jar包。

`java -jar [yourPath].jar help`

中包含了所有必要的帮助。

-  也可以手动运行git-downloader， 在配置好git配置文件后运行命令：`java -jar [yourPath].jar run -g [配置文件路径]`。
-  本包基于不同的git配置文件，可以实现多定时任务平行部署。可用命令：`java -jar [yourPath].jar cron -a -g [配置文件路径]`设置。 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
